package com.florante.rxjava2sampleapp.data;

import com.florante.rxjava2sampleapp.model.Category;
import com.florante.rxjava2sampleapp.repository.CategoryRepository;
import com.florante.rxjava2sampleapp.rest.RestDataSource;

import java.util.ArrayList;

import io.reactivex.Observable;

public class CategoryDataSource implements CategoryRepository{

    RestDataSource mRestDataSource;

    public CategoryDataSource(RestDataSource restDataSource) {
        mRestDataSource = restDataSource;
    }

    @Override
    public Observable<ArrayList<Category>> getCategories() {
        return mRestDataSource.getCategories();
    }
}
