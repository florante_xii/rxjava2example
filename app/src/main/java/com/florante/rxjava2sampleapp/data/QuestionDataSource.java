package com.florante.rxjava2sampleapp.data;

import com.florante.rxjava2sampleapp.model.Question;
import com.florante.rxjava2sampleapp.repository.QuestionRepository;
import com.florante.rxjava2sampleapp.rest.RestDataSource;

import java.util.ArrayList;

import io.reactivex.Observable;

public class QuestionDataSource implements QuestionRepository {

    private RestDataSource mRestDataSource;

    public QuestionDataSource(RestDataSource restDataSource) {
        mRestDataSource = restDataSource;
    }

    @Override
    public Observable<ArrayList<Question>> getQuestions() {
        return mRestDataSource.getQuestions();
    }
}
