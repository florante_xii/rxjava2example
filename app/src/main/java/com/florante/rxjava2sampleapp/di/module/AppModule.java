package com.florante.rxjava2sampleapp.di.module;


import android.app.Application;
import android.content.Context;


import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
    public static final String TAG = AppModule.class.getSimpleName();

    private final Application application;
    private final Context context;

    public AppModule(Application application) {
        this.application = application;
        this.context = application;
    }

    @Singleton
    @Provides
    Application provideApplication() {
        return this.application;
    }

    @Singleton
    @Provides
    Context provideContext() {
        return this.context;
    }

}
