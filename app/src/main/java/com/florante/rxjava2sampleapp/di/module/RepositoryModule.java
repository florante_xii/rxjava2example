package com.florante.rxjava2sampleapp.di.module;

import com.florante.rxjava2sampleapp.data.CategoryDataSource;
import com.florante.rxjava2sampleapp.data.QuestionDataSource;
import com.florante.rxjava2sampleapp.repository.CategoryRepository;
import com.florante.rxjava2sampleapp.repository.QuestionRepository;
import com.florante.rxjava2sampleapp.rest.RestDataSource;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RepositoryModule {

    @Provides
    @Singleton
    CategoryRepository provideCategoryRepository(RestDataSource restDataSource){
        return new CategoryDataSource(restDataSource);
    }

    @Provides
    @Singleton
    QuestionRepository provideQuestionRepository(RestDataSource restDataSource){
        return new QuestionDataSource(restDataSource);
    }
}
