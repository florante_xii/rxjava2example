package com.florante.rxjava2sampleapp.di.module;

import android.content.Context;

import com.florante.rxjava2sampleapp.BuildConfig;
import com.florante.rxjava2sampleapp.rest.ApiService;
import com.florante.rxjava2sampleapp.rest.RestDataSource;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class DataSourceModule {

    @Provides
    @Singleton
    ApiService provideApi(Context context){

        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS);

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor);
        }

        OkHttpClient client = builder.build();

        Gson gson = new GsonBuilder()
                .serializeNulls()
                .create();

        Retrofit apiAdapter = new Retrofit.Builder()
                .baseUrl("http://civilserviceph.x10host.com/civilservicereviewerph/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build();

        return apiAdapter.create(ApiService.class);
    }

    @Provides
    @Singleton
    RestDataSource provideRestDataSource(ApiService apiService) {
        return new RestDataSource(apiService);
    }
}
