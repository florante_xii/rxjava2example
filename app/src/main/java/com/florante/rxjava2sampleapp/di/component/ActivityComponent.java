package com.florante.rxjava2sampleapp.di.component;


import com.florante.rxjava2sampleapp.activity.LandingPageActivity;
import com.florante.rxjava2sampleapp.di.ActivityScope;
import com.florante.rxjava2sampleapp.di.module.CategoryModule;

import dagger.Component;

@ActivityScope
@Component(dependencies = ApplicationComponent.class,
        modules = {
                CategoryModule.class
        })
public interface ActivityComponent {
    void inject(LandingPageActivity loginAndRegisterActivity);
}
