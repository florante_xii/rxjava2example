package com.florante.rxjava2sampleapp.di.module;

import com.florante.rxjava2sampleapp.di.ActivityScope;
import com.florante.rxjava2sampleapp.presenter.CategoryPresenter;
import com.florante.rxjava2sampleapp.repository.CategoryRepository;
import com.florante.rxjava2sampleapp.usecases.CategoryUseCase;

import dagger.Module;
import dagger.Provides;

@Module
public class CategoryModule {

    @ActivityScope
    @Provides
    CategoryUseCase providesCategoryUseCase(CategoryRepository categoryRepository){
        return new CategoryUseCase(categoryRepository);
    }

    @ActivityScope
    @Provides
    CategoryPresenter providesCategoryPresenter(CategoryUseCase categoryUseCase){
        return new CategoryPresenter(categoryUseCase);
    }

}
