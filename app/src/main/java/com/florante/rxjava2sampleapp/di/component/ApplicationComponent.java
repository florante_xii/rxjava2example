package com.florante.rxjava2sampleapp.di.component;

import android.content.Context;


import com.florante.rxjava2sampleapp.BaseApplication;
import com.florante.rxjava2sampleapp.di.module.AppModule;
import com.florante.rxjava2sampleapp.di.module.DataSourceModule;
import com.florante.rxjava2sampleapp.di.module.RepositoryModule;
import com.florante.rxjava2sampleapp.repository.CategoryRepository;
import com.florante.rxjava2sampleapp.repository.QuestionRepository;
import com.florante.rxjava2sampleapp.rest.RestDataSource;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        DataSourceModule.class,
        RepositoryModule.class,
        AppModule.class
})
public interface ApplicationComponent {

    void inject(BaseApplication baseApplication);

    RestDataSource restDataSource();

    CategoryRepository categoryRepository();

    QuestionRepository questionRepository();

}
