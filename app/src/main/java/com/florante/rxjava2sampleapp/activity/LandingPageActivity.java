package com.florante.rxjava2sampleapp.activity;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.florante.rxjava2sampleapp.BaseApplication;
import com.florante.rxjava2sampleapp.R;
import com.florante.rxjava2sampleapp.di.component.ActivityComponent;
import com.florante.rxjava2sampleapp.di.component.DaggerActivityComponent;
import com.florante.rxjava2sampleapp.di.module.CategoryModule;
import com.florante.rxjava2sampleapp.model.Category;
import com.florante.rxjava2sampleapp.presenter.CategoryPresenter;
import com.florante.rxjava2sampleapp.view.CategoryView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LandingPageActivity extends AppCompatActivity implements CategoryView {

    @BindView(R.id.categoriesTextView)
    TextView mCategoriesTextView;

    @Inject
    CategoryPresenter mCategoryPresenter;

    ActivityComponent mActivityComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        initInjector();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing_page);
        ButterKnife.bind(this);

        mCategoryPresenter.bindView(this);
        mCategoryPresenter.getCategories();
    }

    private void initInjector() {
        mActivityComponent = DaggerActivityComponent.builder()
                .categoryModule(new CategoryModule())
                .applicationComponent(((BaseApplication) getApplication()).getAppComponent())
                .build();
        mActivityComponent.inject(this);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showError(@NonNull String errorTag, String message) {

    }

    @Override
    public void onGetCategoriesSuccess(ArrayList<Category> categories) {
        String s = "";
        for (Category category : categories) {
            s = s + category.getCategoryName() + "\n";
        }
        mCategoriesTextView.setText(s);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCategoryPresenter.unBindView();
    }
}
