package com.florante.rxjava2sampleapp.model;

import java.io.Serializable;

/**
 * Created by LanarD on 1/21/2018.
 */

public class Category implements Serializable {
    int id;
    String category_name;

    public Category(int id, String category_name) {
        this.id = id;
        this.category_name = category_name;
    }

    public int getId() {
        return id;
    }


    public String getCategoryName() {
        return category_name;
    }


}

