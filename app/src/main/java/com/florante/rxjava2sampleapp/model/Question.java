package com.florante.rxjava2sampleapp.model;


import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by LanarD on 2/11/2018.
 */

public class Question implements Serializable {

    int id;

    @SerializedName("question_item")
    @Expose
    String question_item;

    @SerializedName("choice_1")
    @Expose
    String choice_1;

    @SerializedName("choice_2")
    @Expose
    String choice_2;

    @SerializedName("choice_3")
    @Expose
    String choice_3;

    @SerializedName("choice_4")
    @Expose
    String choice_4;

    @SerializedName("answer")
    @Expose
    String answer;

    @SerializedName("question_category")
    @Expose
    String question_category;

    boolean isAnswered = false;

    String userAnswer;


    public int getId() {
        return id;
    }

    public String getQuestion_item() {
        return question_item;
    }

    public String getChoice_1() {
        return choice_1;
    }

    public String getChoice_2() {
        return choice_2;
    }

    public String getChoice_3() {
        return choice_3;
    }

    public String getChoice_4() {
        return choice_4;
    }

    public String getAnswer() {
        return answer;
    }

    public String getQuestion_category() {
        return question_category;
    }

    public boolean isAnswered() {
        return isAnswered;
    }

    public void setAnswered(boolean answered) {
        isAnswered = answered;
    }

    public String getUserAnswer() {
        return userAnswer == null || userAnswer.isEmpty() ? "" : userAnswer;
    }

    public void setUserAnswer(String userAnswer) {
        this.userAnswer = userAnswer;
    }
}
