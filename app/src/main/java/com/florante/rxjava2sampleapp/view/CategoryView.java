package com.florante.rxjava2sampleapp.view;

import com.florante.rxjava2sampleapp.model.Category;

import java.util.ArrayList;

public interface CategoryView extends LoadingView {
    void onGetCategoriesSuccess(ArrayList<Category> categories);
}
