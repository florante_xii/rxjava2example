package com.florante.rxjava2sampleapp.view;

public interface LoadingView extends BaseView{
    void showLoading();

    void hideLoading();
}
