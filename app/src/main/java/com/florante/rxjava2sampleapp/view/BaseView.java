package com.florante.rxjava2sampleapp.view;

import android.support.annotation.NonNull;

public interface BaseView {
    void showError(@NonNull String errorTag, String message);
}

