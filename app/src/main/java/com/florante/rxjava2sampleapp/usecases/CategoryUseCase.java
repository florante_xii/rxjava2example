package com.florante.rxjava2sampleapp.usecases;

import com.florante.rxjava2sampleapp.domain.Interactor;
import com.florante.rxjava2sampleapp.model.Category;
import com.florante.rxjava2sampleapp.repository.CategoryRepository;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.functions.Function;


public class CategoryUseCase extends Interactor{

    private CategoryRepository mCategoryRepository;

    public CategoryUseCase(CategoryRepository categoryRepository) {
        mCategoryRepository = categoryRepository;
    }

    public Observable<ArrayList<Category>> getCategories(){
        return mCategoryRepository.getCategories().map(new Function<ArrayList<Category>, ArrayList<Category>>() {
            @Override
            public ArrayList<Category> apply(ArrayList<Category> categories) throws Exception {
                return categories;
            }
        }).compose(this.<ArrayList<Category>>applySchedulers());
    }

}
