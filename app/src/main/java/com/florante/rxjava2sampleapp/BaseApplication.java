package com.florante.rxjava2sampleapp;

import android.app.Application;

import com.florante.rxjava2sampleapp.di.component.ApplicationComponent;
import com.florante.rxjava2sampleapp.di.component.DaggerApplicationComponent;
import com.florante.rxjava2sampleapp.di.module.AppModule;
import com.florante.rxjava2sampleapp.di.module.DataSourceModule;
import com.florante.rxjava2sampleapp.di.module.RepositoryModule;

public class BaseApplication  extends Application {

    private ApplicationComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initAppComponent();
    }

    private void initAppComponent() {
        mAppComponent = DaggerApplicationComponent.builder()
                .appModule(new AppModule(this))
                .dataSourceModule(new DataSourceModule())
                .repositoryModule(new RepositoryModule())
                .build();
        mAppComponent.inject(this);
    }

    public ApplicationComponent getAppComponent() {
        return mAppComponent;
    }



}


