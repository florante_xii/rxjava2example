package com.florante.rxjava2sampleapp.rest;

import com.florante.rxjava2sampleapp.model.Category;
import com.florante.rxjava2sampleapp.model.Question;
import com.florante.rxjava2sampleapp.rest.model.DataWrapper;

import java.util.ArrayList;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface ApiService {

    @GET("api/getcategories.php")
    Observable<DataWrapper<ArrayList<Category>>> getCategories();

    @GET("api/getquestions.php")
    Observable<DataWrapper<ArrayList<Question>>> getQuestions();

}
