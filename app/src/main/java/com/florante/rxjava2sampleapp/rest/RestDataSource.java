package com.florante.rxjava2sampleapp.rest;

import com.florante.rxjava2sampleapp.model.Category;
import com.florante.rxjava2sampleapp.model.Question;
import com.florante.rxjava2sampleapp.repository.CategoryRepository;
import com.florante.rxjava2sampleapp.repository.QuestionRepository;
import com.florante.rxjava2sampleapp.rest.model.DataWrapper;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

public class RestDataSource implements CategoryRepository, QuestionRepository {

    private final ApiService mApiService;

    public RestDataSource(ApiService apiService) {
        mApiService = apiService;
    }

    @Override
    public Observable<ArrayList<Category>> getCategories() {
        return mApiService.getCategories().map(new Function<DataWrapper<ArrayList<Category>>, ArrayList<Category>>() {
            @Override
            public ArrayList<Category> apply(DataWrapper<ArrayList<Category>> arrayListDataWrapper) throws Exception {
                return arrayListDataWrapper.data;
            }
        });
    }

    @Override
    public Observable<ArrayList<Question>> getQuestions() {
        return mApiService.getQuestions().map(new Function<DataWrapper<ArrayList<Question>>, ArrayList<Question>>() {
            @Override
            public ArrayList<Question> apply(DataWrapper<ArrayList<Question>> arrayListDataWrapper) throws Exception {
                return arrayListDataWrapper.data;
            }
        });
    }
}
