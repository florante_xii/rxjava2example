package com.florante.rxjava2sampleapp.presenter;

import android.util.Log;

import com.florante.rxjava2sampleapp.model.Category;
import com.florante.rxjava2sampleapp.usecases.CategoryUseCase;
import com.florante.rxjava2sampleapp.view.CategoryView;

import java.util.ArrayList;

import io.reactivex.observers.DisposableObserver;

public class CategoryPresenter extends BasePresenter<CategoryView>{

    CategoryUseCase mCategoryUseCase;

    public CategoryPresenter(CategoryUseCase categoryUseCase) {
        mCategoryUseCase = categoryUseCase;
    }

    public void getCategories(){
        if (!isViewBound()) return;
        view.showLoading();
        DisposableObserver<ArrayList<Category>> arrayListDisposableObserver = new DisposableObserver<ArrayList<Category>>() {
            @Override
            public void onNext(ArrayList<Category> categories) {
                view.hideLoading();
                view.onGetCategoriesSuccess(categories);
            }

            @Override
            public void onError(Throwable e) {
                try {
                    view.hideLoading();
                    view.showError("","Error");
                } catch (Throwable err) {
                    Log.e(TAG, "onError: ", err);
                }
            }

            @Override
            public void onComplete() {
            }
        };
        mCategoryUseCase.getCategories().subscribe(arrayListDisposableObserver);
        unsubscribeOnUnbindView(arrayListDisposableObserver);
    }
}
