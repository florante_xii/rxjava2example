package com.florante.rxjava2sampleapp.presenter;


import android.support.annotation.NonNull;
import android.util.Log;


import com.florante.rxjava2sampleapp.view.BaseView;


import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class BasePresenter<T extends BaseView> {

    public static final String TAG = BasePresenter.class.getSimpleName();
    public static final String ERROR_PARSE = "Error parsing data";
    public static final String ERROR_GENERIC = "Oops… Something wrong happened.";
    public static final String ERROR_CONNECTION = "Oops… There's something wrong with your connection.";

    protected T view;

    private final CompositeDisposable subscriptionsToUnsubscribeOnUnbindView = new CompositeDisposable();

    protected final void unsubscribeOnUnbindView(@NonNull Disposable disposable) {
        subscriptionsToUnsubscribeOnUnbindView.add(disposable);
    }

    public void bindView(T view) {
        this.view = view;
    }

    public void unBindView() {
        this.view = null;
        subscriptionsToUnsubscribeOnUnbindView.clear();
    }

    public boolean isViewBound() {
        return view != null;
    }

//    protected ErrorResponse getErrorResponse(Throwable e) throws Exception {
//        Log.d(TAG, "getErrorResponse: ", e);
//        if (e instanceof HttpException) {
//            try {
//                HttpException error = (HttpException) e;
//                String errorBody = error.response().errorBody().string();
//                return RestErrorHandler.parseErrorDetails(errorBody);
//            } catch (JSONException | IOException e1) {
//                Log.e(TAG, e1.getMessage(), e1);
//                return new ErrorResponse("", 0, ERROR_PARSE);
//            }
//        } else if (e instanceof NoConnectivityException) {
//            return new ErrorResponse("", -1, ERROR_CONNECTION);
//        } else if (e instanceof NoResultsException) {
//            return new ErrorResponse("", -2, e.getMessage());
//        } else if (e instanceof SSLHandshakeException) {
//            return new ErrorResponse("", -3, "Connection timed out. Please check your internet connection and try again.");
//        } else if (e instanceof SocketTimeoutException) {
//            return new ErrorResponse("", -4, "Connection timed out. Please check your internet connection and try again.");
//        }
//        Sentry.capture(e);
//        return new ErrorResponse("", -5, ERROR_GENERIC);
//    }
}
