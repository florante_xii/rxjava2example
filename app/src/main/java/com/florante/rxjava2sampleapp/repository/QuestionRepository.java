package com.florante.rxjava2sampleapp.repository;

import com.florante.rxjava2sampleapp.model.Category;
import com.florante.rxjava2sampleapp.model.Question;

import java.util.ArrayList;

import io.reactivex.Observable;


public interface QuestionRepository {
    Observable<ArrayList<Question>> getQuestions();
}
