package com.florante.rxjava2sampleapp.repository;

import com.florante.rxjava2sampleapp.model.Category;

import java.util.ArrayList;

import io.reactivex.Observable;

public interface CategoryRepository {
    Observable<ArrayList<Category>> getCategories();
}
